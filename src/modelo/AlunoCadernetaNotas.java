package modelo;

import java.util.List;

public class AlunoCadernetaNotas {
	private String matricula;
	private String nome;
	private Double media;
	private List<Double> notas;
	private List<Boolean> faltas;

	public Double getMedia() {
		return media;
	}

	public void setMedia(Double media) {
		this.media = media;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Double> getNotas() {
		return notas;
	}

	public void setNotas(List<Double> notas) {
		this.notas = notas;
	}

	public List<Boolean> getFaltas() {
		return faltas;
	}

	public void setFaltas(List<Boolean> faltas) {
		this.faltas = faltas;
	}

	public Object[] getValuesAsArrayOfNota() {
		Object[] values = new Object[3 + notas.size()];
		values[0] = matricula;
		values[1] = nome;
		for (int i = 0; i < notas.size(); i++) {
			values[i + 2] = notas.get(i);
		}
		values[3 + notas.size() - 1] = null;
		return values;
	}

	public String[] getNamesAsArrayOfNota() {
		String[] names = new String[3 + notas.size()];
		names[0] = "Matrícula";
		names[1] = "Nome";
		for (int i = 0; i < notas.size(); i++) {
			names[i + 2] = "Nota " + (i + 1);
		}
		names[3 + notas.size() - 1] = "Média";
		return names;
	}

	public Object[] getValuesAsArrayFalta() {
		Object[] values = new Object[3 + faltas.size()];
		values[0] = matricula;
		values[1] = nome;
		for (int i = 0; i < faltas.size(); i++) {
			values[i + 2] = faltas.get(i);
		}
		values[3 + faltas.size() - 1] = 0;
		return values;
	}

	public String[] getNamesAsArrayOfFalta() {
		String[] names = new String[3 + faltas.size()];
		names[0] = "Matrícula";
		names[1] = "Nome";
		for (int i = 0; i < faltas.size(); i++) {
			names[i + 2] = "Aula " + (i + 1);
		}
		names[3 + faltas.size() - 1] = "Frequência";
		return names;
	}
}
