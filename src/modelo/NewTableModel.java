package modelo;

import javax.swing.table.DefaultTableModel;

public class NewTableModel extends DefaultTableModel {

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		Object value = getValueAt(0,columnIndex);
		if (value != null){
			return value.getClass();
		}else{
			return Object.class;
		}
		
	}

}
