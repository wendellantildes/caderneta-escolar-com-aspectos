package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import net.miginfocom.swing.MigLayout;
import entidade.Materia;
import entidade.Serie;
import entidade.Turma;

public class ClassComponent implements ContentProvider {

	private JComboBox comboBoxSerie;
	private ComboBoxModel comboBoxModelSerie;
	private JComboBox comboBoxTurma;
	private ComboBoxModel comboBoxModelTurma;
	private JComboBox comboBoxMateria;
	private ComboBoxModel comboBoxModelMateria;
	private JLabel labelSerie;
	private JLabel labelTurma;
	private JLabel labelMateria;
	private JScrollPane scrollPane;
	private JPanel panel;
	private Estado estadoAtual;
	
	public enum Estado{
		TELA_AVALIACOES,
		TELA_CADERNETAS,
		TELA_RELATORIO
	}

	public ClassComponent(List<Serie> series, List<Turma> turmas,
			List<Materia> materias, Integer qt_avaliacoes, Estado estado) {
		MigLayout layout = new MigLayout();
		panel = new JPanel();
		panel.setLayout(layout);
		this.estadoAtual = estado;
		iniciaComponentes();
		preencheComboBox(series, turmas, materias, qt_avaliacoes);
	}

	private void iniciaComponentes() {
		this.labelSerie = new JLabel("Série: ");
		this.labelTurma = new JLabel("Turma: ");
		comboBoxModelSerie = new DefaultComboBoxModel();
		this.comboBoxSerie = new JComboBox(comboBoxModelSerie);
		comboBoxSerie.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				trocarTurmas();
			}

		});
		comboBoxModelTurma = new DefaultComboBoxModel();
		this.comboBoxTurma = new JComboBox(comboBoxModelTurma);
		this.comboBoxTurma.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				trocarMaterias();

			}
		});
		this.panel.add(labelSerie, "split 2");
		this.panel.add(comboBoxSerie);
		this.panel.add(labelTurma);
		this.panel.add(comboBoxTurma, "wrap");
		this.labelMateria = new JLabel("Matéria: ");
		comboBoxModelMateria = new DefaultComboBoxModel();
		this.comboBoxMateria = new JComboBox(comboBoxModelMateria);
		this.comboBoxMateria.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				trocarNAvaliacoes();
			}
		});
		this.panel.add(labelMateria, "split 2");
		this.panel.add(comboBoxMateria, "wrap");
		this.scrollPane = new JScrollPane(panel);
	}

	private void preencheComboBox(List<Serie> series, List<Turma> turmas,
			List<Materia> materias, Integer qt_avaliacoes) {
		int i = 0;
		for (Materia materia : materias) {
			comboBoxMateria.insertItemAt(materia, i);
			i++;
		}

		i = 0;
		for (Serie serie : series) {
			comboBoxSerie.insertItemAt(serie, i);
			i++;
		}

		i = 0;
		for (Turma turma : turmas) {
			System.out.println("sss: " + turma);
			comboBoxTurma.insertItemAt(turma, i);
			i++;
		}
		comboBoxTurma.setSelectedIndex(0);
		comboBoxSerie.setSelectedIndex(0);
		comboBoxMateria.setSelectedIndex(0);
	}

	private void trocarTurmas() {

	}

	private void trocarMaterias() {
		// TODO Auto-generated method stub

	}

	private void trocarNAvaliacoes() {
		// TODO Auto-generated method stub

	}

	public Serie getSerieSelecionada() {
		return (Serie) comboBoxSerie.getSelectedItem();
	}

	public Turma getTurmaSelecionada() {
		System.out.println((Turma) comboBoxTurma.getSelectedItem());
		return (Turma) comboBoxTurma.getSelectedItem();
	}

	public Materia getMateriaSelecionada() {
		return (Materia) comboBoxMateria.getSelectedItem();
	}

	public void setElementosTurma(List<Turma> turmas) {
		comboBoxTurma.removeAllItems();
		int i = 0;
		for (Turma turma : turmas) {
			comboBoxTurma.insertItemAt(turma, i);
			i++;
		}
		comboBoxTurma.setSelectedIndex(0);
		System.out.println("mudado");
	}

	public void setElementosMaterias(List<Materia> materias) {
		comboBoxMateria.removeAllItems();
		int i = 0;
		for (Materia materia : materias) {
			comboBoxMateria.insertItemAt(materia, i);
			i++;
		}
		comboBoxMateria.setSelectedIndex(0);
		System.out.println("mudado");
	}

	@Override
	public JComponent getContent() {
		return panel;
//		return this.scrollPane;
	}

	public Estado getEstadoAtual() {
		return estadoAtual;
	}

}
