package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import net.miginfocom.swing.MigLayout;

import org.jdesktop.swingx.JXHyperlink;

public class TelaPrincipal extends JFrame {

	private JToolBar toolbar;
	private JXHyperlink voltar;
	private JXHyperlink hiperlink;
	private String userName;
	private MyPanel panelPai;

	public TelaPrincipal(String username) {
		super("Dj School");
		this.userName = username;
		iniciaToolBar();
		iniciaMyPanel();
		configurarFrame();
	}

	private void iniciaToolBar() {
		toolbar = new JToolBar();
		toolbar.setFloatable(false);
		voltar = new JXHyperlink();
		voltar.setText("Voltar");
		voltar.setVisible(false);
		voltar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				voltarTelaProfessor();
				
			}
		});
		toolbar.add(voltar);
		toolbar.add(Box.createHorizontalGlue());
		toolbar.add(new JLabel("Professor: " + this.userName));
		hiperlink = new JXHyperlink();
		hiperlink.setText("Sair");
		hiperlink.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fecha();
			}
		});
		toolbar.add(hiperlink);
		this.setLayout(new BorderLayout());
		this.add(toolbar, BorderLayout.NORTH);

	}

	private void iniciaMyPanel() {
		panelPai = new MyPanel();
		panelPai.setLayout(new BorderLayout());
		this.add(panelPai,BorderLayout.CENTER);
	}

	private void configurarFrame() {
		this.setSize(600, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//this.setResizable(false);
	}

	public void fecha() {
		
	}
	private void voltarTelaProfessor() {
		voltar.setVisible(false);
	}
	public void ativarVoltar(){
		voltar.setVisible(true);
	}
	public void setContent(JComponent component){
		panelPai.setContent(component);
	}

	public String getUserName() {
		return userName;
	}

	private class MyPanel extends JPanel {
		public void setContent(JComponent component) {
			this.removeAll();
			this.add(component,BorderLayout.CENTER);
			this.repaint();
			this.validate();

		}
	}

	public static void main(String[] args) {
		TelaPrincipal tp = new TelaPrincipal("ana");
		tp.setVisible(true);
	}
}
