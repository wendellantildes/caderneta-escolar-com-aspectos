package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import modelo.AlunoCadernetaNotas;
import modelo.NewTableModel;
import net.miginfocom.swing.MigLayout;

public class Caderneta implements ContentProvider {
	private JLabel labelCaderneta;
	private JLabel labelProfessor;
	private JLabel labelMateria;
	private JLabel labelSerie;
	private JLabel labelTurma;
	private JTable tableNotas;
	private DefaultTableModel tableModelNotas;
	private JTable tableFaltas;
	private DefaultTableModel tableModelFaltas;
	private JPanel panel;
	private JScrollPane scrollPaneNotas;
	private JScrollPane scrollPaneFaltas;
	private AlunoCadernetaNotas[] alunosNotas;
	private JButton buttonTabelaNotas;
	private JButton buttonTabelaFaltas;
	private JButton buttonSalvarAlteracoes;
	private JButton buttonFecharCaderneta;

	public Caderneta(String professor, String serie, String turma,
			String materia, AlunoCadernetaNotas[] alunosNotas) {
		this.panel = new JPanel(new MigLayout());
		this.iniciaComponentes(professor, serie, turma, materia);
		this.alunosNotas = alunosNotas;

		preencheTabelaNotas();
		preencherTabelaFaltas();
	}

	private void preencheTabelaNotas() {
		for (Object o : alunosNotas[0].getNamesAsArrayOfNota()) {
			this.tableModelNotas.addColumn(o);
		}
		for (AlunoCadernetaNotas alunoCadernetaNotas : alunosNotas) {
			// System.out.println(alunoCadernetaNotas.getNome());
			this.tableModelNotas.addRow(alunoCadernetaNotas
					.getValuesAsArrayOfNota());
		}
		for (int row = 0; row < tableModelNotas.getRowCount(); row++) {
			calculaMedia(row, 2);
		}
	}

	private void preencherTabelaFaltas() {
		for (Object o : alunosNotas[0].getNamesAsArrayOfFalta()) {
			this.tableModelFaltas.addColumn(o);
		}
		for (AlunoCadernetaNotas alunoCadernetaNotas : alunosNotas) {
			this.tableModelFaltas.addRow(alunoCadernetaNotas
					.getValuesAsArrayFalta());
		}
	}

	private void iniciaComponentes(String professor, String serie,
			String turma, String materia) {
		labelCaderneta = new JLabel("Caderneta");
		this.labelCaderneta.setFont(this.labelCaderneta.getFont().deriveFont(
				16f));
		this.panel.add(labelCaderneta, "wrap");
		labelProfessor = new JLabel("Professor: " + professor);
		labelSerie = new JLabel("Série: " + serie);
		labelTurma = new JLabel("Turma: " + turma);
		labelMateria = new JLabel("Matéria: " + materia);
		this.buttonTabelaNotas = new JButton("Tabela de Notas");
		this.buttonTabelaNotas.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				trocarTabelaAtualPorTabelaNotas();
			}

		});
		this.buttonTabelaFaltas = new JButton("Tabela de Faltas");
		this.buttonTabelaFaltas.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				trocarTabelaAtualPorTabelaFaltas();

			}

		});
		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(buttonTabelaNotas);
		buttonGroup.add(this.buttonTabelaFaltas);
		tableModelNotas = new NewTableModel();
		tableNotas = new JTable(tableModelNotas);
		tableModelNotas.addTableModelListener(new TableModelListener() {

			@Override
			public void tableChanged(TableModelEvent e) {
				if (e.getType() == TableModelEvent.UPDATE) {
					if (e.getFirstRow() > -1 && e.getColumn() != tableModelNotas.getColumnCount() - 1) {
						Object valorObj = tableModelNotas.getValueAt(e.getFirstRow(), e.getColumn());
						Double valor;
						if (valorObj != null) {
							if (valorObj instanceof String) {
								try {
									valor = Double.parseDouble((String) valorObj);
								} catch (NumberFormatException ex) {
									tableModelNotas.setValueAt(null, e.getFirstRow(), e.getColumn());
									return;
								}
							} else {
								valor = (Double) valorObj;
							}
							if (valor < 0 || valor > 10) {
								tableModelNotas.setValueAt(null, e.getFirstRow(), e.getColumn());
								return;
							}
						}
						calculaMedia(e.getFirstRow(), e.getColumn());
					}
				}
			}
		});
		this.scrollPaneNotas = new JScrollPane(tableNotas);

		this.panel.setSize(500, 500);
		this.panel.add(labelProfessor, "wrap");
		this.panel.add(labelSerie);
		this.panel.add(labelTurma, "wrap");
		this.panel.add(labelMateria, "wrap");
		this.panel.add(Box.createHorizontalGlue());
		this.panel.add(this.buttonTabelaNotas);
		this.panel.add(this.buttonTabelaFaltas);
		this.panel.add(Box.createHorizontalGlue(), "wrap");
		this.panel.add(tableNotas.getTableHeader(), "span,grow x");
		this.panel.add(tableNotas, "span,grow x,wrap");
		this.buttonSalvarAlteracoes = new JButton("Salvar Alterações");
		this.buttonSalvarAlteracoes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				salvarAlteracoes();
			}
		});

		this.buttonFecharCaderneta = new JButton("Fechar Caderneta");
		this.buttonFecharCaderneta.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fecharCaderneta();
			}
		});
		this.panel.add(this.buttonSalvarAlteracoes);
		this.panel.add(this.buttonFecharCaderneta);
		this.tableModelFaltas = new NewTableModel();
		this.tableFaltas = new JTable(tableModelFaltas);
		this.scrollPaneFaltas = new JScrollPane(tableFaltas);
	}

	private void fecharCaderneta() {}

	private void calculaMedia(int linha, int coluna) {
		try {
			int qtNotas = 0;
			double media = 0d;
			for (int column = 2; column < tableModelNotas.getColumnCount() - 1; column++) {
				Object o = tableModelNotas.getValueAt(linha, column);
				if (o != null) {
					if (o instanceof String) {
						media += Double.parseDouble((String) o);
					} else {
						media += (Double) o;
					}
					qtNotas++;
				}
			}
			if (qtNotas != 0) {
				tableModelNotas.setValueAt(media / qtNotas, linha, tableModelNotas.getColumnCount() - 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void salvarAlteracoes() {
	}

	private void trocarTabelaAtualPorTabelaFaltas() {
		this.panel.remove(tableNotas.getTableHeader());
		this.panel.remove(tableNotas);
		this.panel.remove(this.buttonSalvarAlteracoes);
		this.panel.remove(this.buttonFecharCaderneta);
		this.panel.add(tableFaltas.getTableHeader(), "span,grow x");
		this.panel.add(tableFaltas, "span,grow x");
		this.panel.add(this.buttonSalvarAlteracoes);
		this.panel.add(this.buttonFecharCaderneta);
		this.panel.validate();
	}

	private void trocarTabelaAtualPorTabelaNotas() {
		this.panel.remove(tableFaltas.getTableHeader());
		this.panel.remove(tableFaltas);
		this.panel.remove(this.buttonSalvarAlteracoes);
		this.panel.remove(this.buttonFecharCaderneta);
		this.panel.add(tableNotas.getTableHeader(), "span,grow x");
		this.panel.add(tableNotas, "span,grow x");
		this.panel.add(this.buttonSalvarAlteracoes);
		this.panel.add(this.buttonFecharCaderneta);
		this.panel.validate();
	}

	@Override
	public JComponent getContent() {
		return new JScrollPane(this.panel);
	}

	public DefaultTableModel getTableModelNotas() {
		return tableModelNotas;
	}

	public DefaultTableModel getTableModelFaltas() {
		return tableModelFaltas;
	}

	 public static void main(String[] args) {
	 JFrame f = new JFrame();
	 f.setLayout(new BorderLayout());
	 AlunoCadernetaNotas[] a = new AlunoCadernetaNotas[1];
	 a[0] = new AlunoCadernetaNotas();
	 ArrayList<Double> d = new ArrayList<Double>();
	 d.add(null);
	 d.add(null);
	 a[0].setMatricula("11");
	 a[0].setNome("ana");
	 a[0].setNotas(d);
	 ArrayList<Boolean> b = new ArrayList<Boolean>();
	 b.add(false);
	 a[0].setFaltas(b);
	 Caderneta c = new Caderneta("Mário", "Primeira", "A", "Português", a);
	 f.add(c.getContent(), BorderLayout.CENTER);
	 f.setSize(600, 600);
	 f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	 f.setVisible(true);
	 }

}
