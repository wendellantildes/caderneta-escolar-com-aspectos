package gui;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;

import org.apache.commons.codec.digest.DigestUtils;

public class Login extends JFrame {
	private static final long serialVersionUID = 1L;

	private JLabel labelMatricula;
	private JLabel labelSenha;
	private JTextField textFieldMatricula;
	private JPasswordField passwordField;
	private JButton buttonLogin;

	public Login() {
		super("Login");
		MigLayout layout = new MigLayout();
		this.setLayout(layout);
		iniciaComponentes();
		configurarFrame();
	}

	private void iniciaComponentes() {
		this.labelMatricula = new JLabel("Matrícula: ");
		this.labelSenha = new JLabel("Senha");
		this.textFieldMatricula = new JTextField(25);
		this.passwordField = new JPasswordField(25);
		this.buttonLogin = new JButton("Logar");
		buttonLogin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				logar();
			}
		});
		this.add(labelMatricula);
		this.add(textFieldMatricula, "wrap");
		this.add(labelSenha);
		this.add(passwordField, "wrap");
		this.add(Box.createHorizontalGlue());
		this.add(buttonLogin);
	}

	private void configurarFrame() {
		this.setSize(250, 120);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		center();
	}
	
	private void center() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Point p = new Point((int) screenSize.getWidth() / 2 - this.getWidth(), (int) screenSize.getHeight() / 2 - this.getHeight());
		this.setLocation(p);
	}

	private void logar() {
	}

	public String getMatricula() {
		return textFieldMatricula.getText();
	}

	public String getSenha() {
		return DigestUtils.md5Hex((String.valueOf(passwordField.getPassword())));
	}

	public static void main(String[] args) {
		Login l = new Login();
		l.setVisible(true);
	}
}
