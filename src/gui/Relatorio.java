package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import net.miginfocom.swing.MigLayout;
import entidade.Materia;
import entidade.Serie;
import entidade.Turma;
import gui.ClassComponent.Estado;

public class Relatorio implements ContentProvider {
	private ClassComponent componente;
	private JScrollPane scrollPane;
	private JPanel panel;
	private JButton buttonGerarRelatorioDiarioDeClasse;
	private JButton buttonGerarRelatorioFinal;

	public Relatorio(List<Serie> series, List<Turma> turmas,
			List<Materia> materias, Integer qt_avaliacoes) {
		MigLayout layout = new MigLayout();
		componente = new ClassComponent(series, turmas, materias, qt_avaliacoes, Estado.TELA_RELATORIO);
		panel = new JPanel();
		panel.setLayout(layout);
		iniciaComponentes();
	}

	private void iniciaComponentes() {
		this.panel.add(componente.getContent(), "wrap");
		buttonGerarRelatorioDiarioDeClasse = new JButton(
				"Gerar Diário de Classe");
		buttonGerarRelatorioDiarioDeClasse.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				geraRelatorioDiarioDeClasse();				
			}
		});
			
		this.panel.add(buttonGerarRelatorioDiarioDeClasse, "split 2");
		buttonGerarRelatorioFinal = new JButton("Gerar Relatório Final");
		buttonGerarRelatorioFinal.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				geraRelatorioFinal();
				
			}
		});
		this.panel.add(buttonGerarRelatorioFinal);
		this.scrollPane = new JScrollPane(panel);
	}

	public void geraRelatorioFinal() {

	}

	public void geraRelatorioDiarioDeClasse() {

	}

	public Turma getTurmaSelecionada() {
		return componente.getTurmaSelecionada();
	}
	
	public Serie getSerieSelecionada() {
		return componente.getSerieSelecionada();
	}
	
	public Materia getMateriaSelecionada() {
		return componente.getMateriaSelecionada();
	}
	
	@Override
	public JComponent getContent() {
		// TODO Auto-generated method stub
		return this.scrollPane;
	}

	public static void main(String[] args) {
		// JFrame f = new JFrame();
		// f.setLayout(new BorderLayout());
		// Relatorio t = new Relatorio();
		// f.add(t.getContent(),BorderLayout.CENTER);
		// f.setSize(500, 500);
		// f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		// f.setVisible(true);
	}

}
