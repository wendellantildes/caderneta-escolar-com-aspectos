package gui;

import javax.swing.JComponent;


public interface ContentProvider {
	public JComponent getContent();
}
