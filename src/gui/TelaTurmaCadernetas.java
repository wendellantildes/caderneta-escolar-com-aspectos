package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import net.miginfocom.swing.MigLayout;
import entidade.Materia;
import entidade.Serie;
import entidade.Turma;
import gui.ClassComponent.Estado;

public class TelaTurmaCadernetas implements ContentProvider {
	private ClassComponent componente;
	private JButton buttonAbrirCaderneta;
	private JScrollPane scrollPane;
	private JPanel panel;
	

	public TelaTurmaCadernetas(List<Serie> series, List<Turma> turmas,
			List<Materia> materias, Integer qt_avaliacoes) {
		MigLayout layout = new MigLayout();
		componente = new ClassComponent(series, turmas, materias, qt_avaliacoes, Estado.TELA_CADERNETAS);
		panel = new JPanel();
		panel.setLayout(layout);
		iniciaComponentes();
	}

	private void iniciaComponentes() {
		this.panel.add(componente.getContent(),"wrap");
		buttonAbrirCaderneta = new JButton("Abrir Caderneta");
		buttonAbrirCaderneta.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				abrirCaderneta();
			}
		});
		this.panel.add(buttonAbrirCaderneta);
		this.scrollPane = new JScrollPane(panel);
	}

	

	private void abrirCaderneta() {}

	@Override
	public JComponent getContent() {
		return this.scrollPane;
	}
	
	public Turma getTurmaSelecionada() {
		return componente.getTurmaSelecionada();
	}

	public Materia getMateriaSelecionada() {
		return componente.getMateriaSelecionada();
	}
	
	public Serie getSerieSelecionada() {
		return componente.getSerieSelecionada();
	}

}
