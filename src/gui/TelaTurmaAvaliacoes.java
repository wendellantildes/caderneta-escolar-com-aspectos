package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;
import entidade.Materia;
import entidade.Serie;
import entidade.Turma;
import gui.ClassComponent.Estado;

public class TelaTurmaAvaliacoes implements ContentProvider {
	private JLabel turmaAvaliacoes;
	private ClassComponent componente;
	private JLabel labelNAvaliacoes;
	private JTextField textFieldNAvaliacoes;
	private JButton buttonDefinirNAvalicaoes;
	private JScrollPane scrollPane;
	private JPanel panel;
	

	public TelaTurmaAvaliacoes(List<Serie> series, List<Turma> turmas,
			List<Materia> materias, Integer qt_avaliacoes) {
		MigLayout layout = new MigLayout();
		componente = new ClassComponent(series, turmas, materias, qt_avaliacoes, Estado.TELA_AVALIACOES);
		panel = new JPanel();
		panel.setLayout(layout);
		iniciaComponentes();
		textFieldNAvaliacoes.setText(String.valueOf(qt_avaliacoes));
	}

	private void iniciaComponentes() {
		this.turmaAvaliacoes = new JLabel("Definição da quantidade de avaliações");
		this.turmaAvaliacoes.setFont(this.turmaAvaliacoes.getFont().deriveFont(16f));
		this.panel.add(this.turmaAvaliacoes,"wrap");
		this.panel.add(componente.getContent(),"wrap");
		this.labelNAvaliacoes = new JLabel("Número de Avaliações: ");
		this.textFieldNAvaliacoes = new JTextField(4);
		this.panel.add(labelNAvaliacoes,"split 2");
		this.panel.add(textFieldNAvaliacoes, "wrap");
		buttonDefinirNAvalicaoes = new JButton("Definir Avaliações");
		buttonDefinirNAvalicaoes.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				definirNAvaliacoes();
			}
		});
		this.panel.add(buttonDefinirNAvalicaoes);
		this.scrollPane = new JScrollPane(panel);
	}

	

	private void definirNAvaliacoes() {
		
	}

	
	public String getQtAvaliacoes(){
		return textFieldNAvaliacoes.getText();
	}


	public void setNAvaliacoes(Integer valor){
		textFieldNAvaliacoes.setText(String.valueOf(valor));
	}
	@Override
	public JComponent getContent() {
		return this.scrollPane;
	}

	public static void main(String[] args) {
		// JFrame f = new JFrame();
		// f.setLayout(new BorderLayout());
		// TelaTurmaAvaliacoes t = new TelaTurmaAvaliacoes();
		// f.add(t.getContent(), BorderLayout.CENTER);
		// f.setSize(300, 300);
		// f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		// f.setVisible(true);
	}

	public Turma getTurmaSelecionada() {
		return componente.getTurmaSelecionada();
	}

	public Materia getMateriaSelecionada() {
		return componente.getMateriaSelecionada();
	}

}
