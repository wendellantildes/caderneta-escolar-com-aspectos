package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import net.miginfocom.swing.MigLayout;

import org.jdesktop.swingx.JXHyperlink;

public class TelaProfessor implements ContentProvider {
	private JXHyperlink linkTurmaAvaliacoes;
	private JXHyperlink linkTurmaNotaFaltas;
	private JXHyperlink linkTurmaGerarRelatorios;
	private JPanel panel;
	private JScrollPane scrollPane;

	public TelaProfessor() {
		MigLayout layout = new MigLayout();
		panel = new JPanel();
		panel.setLayout(layout);
		iniciaComponentes();
	}

	private void iniciaComponentes() {
		this.linkTurmaAvaliacoes = new JXHyperlink();
		this.linkTurmaAvaliacoes
				.setText("Definir quantidade de avaliações para uma turma");
		linkTurmaAvaliacoes.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				telaTurmaAvaliacoes();
			}			
		});
		this.panel.add(this.linkTurmaAvaliacoes, "wrap");
		this.linkTurmaNotaFaltas = new JXHyperlink();
		this.linkTurmaNotaFaltas
				.setText("Preencher notas e faltas de uma turma");
		linkTurmaNotaFaltas.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				telaTurmaNotasFaltas();				
			}
		});
		this.panel.add(this.linkTurmaNotaFaltas, "wrap");
		this.linkTurmaGerarRelatorios = new JXHyperlink();
		this.linkTurmaGerarRelatorios
				.setText("Gerar Relatórios de uma turma");
		linkTurmaGerarRelatorios.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				telaTurmaGerarRelatorios();				
			}
		});
		this.panel.add(this.linkTurmaGerarRelatorios, "wrap");
		this.scrollPane = new JScrollPane(panel);
	}

	@Override
	public JComponent getContent() {
		// TODO Auto-generated method stub
		return this.scrollPane;
	}
	private void telaTurmaAvaliacoes() {

	}
	private void telaTurmaNotasFaltas() {

	}
	private void telaTurmaGerarRelatorios() {

	}
	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setLayout(new BorderLayout());
		TelaProfessor t = new TelaProfessor();
		f.add(t.getContent(), BorderLayout.CENTER);
		f.setSize(400, 400);
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.setVisible(true);
	}

}
