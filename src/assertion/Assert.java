package assertion;

import java.util.regex.Pattern;

public class Assert {
	public static void isTrue(String comment, boolean test) {
		if (!test) {
			throw new RuntimeException("Assertion failed: " + comment);
		}
	}
	public static boolean apenasTexto(String texto){
		if (texto.isEmpty()) return false;
		if (!Pattern.matches("[\\p{L}]+", texto.trim()
				.substring(0, 1))) {
			return false;
		}
		return true;
	}
	public static boolean apenasNumero(String texto){
		if (texto.length() == 0) return false;
		if (!Pattern.matches("[0-9]+", texto.trim())) {
			return false;
		}
		return true;
	}
	public static boolean senhaVazia(String senha){
		return senha.equals("d41d8cd98f00b204e9800998ecf8427e")? true: false;
	}
	public static boolean stringVazia(String texto){
		return texto.isEmpty() ? true: false;
	}
	
}
