package aspectos;

import gui.TelaPrincipal;
import gui.TelaTurmaAvaliacoes;

import java.io.File;
import java.io.FileOutputStream;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import org.apache.log4j.HTMLLayout;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.WriterAppender;

public aspect LoggAspecto {
	private HTMLLayout layout = new HTMLLayout();
	private WriterAppender appender = null;
	private FileOutputStream output;
	private Logger logger = Logger.getLogger("Acesso ao sistema");
	private String filename = "acesso.html";
	private String userName;

	public LoggAspecto() {
		try {
			if (new File(filename).exists()) {
				output = new FileOutputStream(filename, true);
				
			} else {
				output = new FileOutputStream(filename);
			}
			// cria o arquivo HTML

			// cria um WriterAppender passando o layout e o file
			appender = new WriterAppender(layout, output);
		} catch (Exception e) {
			// logando uma exception, caso ocorra
			logger.error("Erro: " + e.getMessage());
		}
		logger.addAppender(appender);
	}

	pointcut logou(TelaPrincipal t):
	 initialization(gui.TelaPrincipal.new(..)) && target(t);

	after(TelaPrincipal t):logou(t){
		logger.setLevel((Level) Level.INFO);
		userName = t.getUserName().toUpperCase();
		logger.info(t.getUserName().toUpperCase() + " logou");
	}

	pointcut logout(TelaPrincipal t):
		 within(gui.TelaPrincipal) && call(public void fecha()) && target(t);

	after(TelaPrincipal t):logout(t){
		logger.setLevel((Level) Level.INFO);
		logger.info(t.getUserName().toUpperCase() + " fez logout");
	}
	
}
