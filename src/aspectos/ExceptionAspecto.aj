package aspectos;

import java.io.File;
import java.io.FileOutputStream;

import javax.swing.JOptionPane;

import org.apache.log4j.HTMLLayout;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.WriterAppender;

public aspect ExceptionAspecto {
	private HTMLLayout layout = new HTMLLayout();
	private WriterAppender appender = null;
	private FileOutputStream output;
	private Logger logger = Logger
			.getLogger("Erro de Acesso ao Banco de Dados");
	private String filename = "erroBD.html";

	public ExceptionAspecto() {
		try {
			if (new File(filename).exists()) {
				output = new FileOutputStream(filename, true);
			} else {
				output = new FileOutputStream(filename);
			}
			// cria o arquivo HTML

			// cria um WriterAppender passando o layout e o file
			appender = new WriterAppender(layout, output);
		} catch (Exception e) {
			// logando uma exception, caso ocorra
			logger.error("Erro: " + e.getMessage());
		}
		logger.addAppender(appender);

	}

	pointcut tratamentoExcecaoSQL(java.sql.SQLException e): handler(java.sql.SQLException) && within(persist.*) && args(e);

	before(java.sql.SQLException e) : tratamentoExcecaoSQL(e){
		logger.setLevel((Level) Level.ERROR);
		logger.error(e.getMessage());
		JOptionPane.showMessageDialog(null, "Erro com o banco de dados. Verifique arquivo de log", "Erro", JOptionPane.ERROR_MESSAGE);
	}
}
