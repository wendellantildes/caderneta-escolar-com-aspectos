package aspectos;

public aspect DesignAspecto {
		
	declare error: call(gui.*.new(..)) && !within(aspectos.*) && !within(gui.*):
		"Só quem pode instanciar uma classe do pacote gui é o pacote aspectos ou o pacote gui";
	
	declare error: call(persist.*.new(..)) && !within(aspectos.*) && !within(persist.*):
		"Só quem pode instanciar uma classe do pacote persist é o pacote aspectos";
	
}
