package aspectos;

import entidade.Materia;
import entidade.RelatorioFinalBean;
import entidade.Serie;
import entidade.Turma;
import gui.Caderneta;
import gui.ClassComponent;
import gui.ClassComponent.Estado;
import gui.Login;
import gui.Relatorio;
import gui.TelaPrincipal;
import gui.TelaProfessor;
import gui.TelaTurmaAvaliacoes;
import gui.TelaTurmaCadernetas;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;

import modelo.AlunoCadernetaNotas;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import persist.Connect;
import persist.DaoProfessor;
import persist.DaoReport;
import persist.DaoSql;
import assertion.Assert;

public aspect GuiAscpecto {
	private TelaPrincipal telaPrincipal;
	private TelaProfessor telaProfessor;
	private TelaTurmaAvaliacoes telaTurmaAvaliacoes;
	private TelaTurmaCadernetas telaTurmaCadernetas;
	private Caderneta caderneta;
	private Relatorio relatorio;
	private Integer matriculaProfessor;
	private Integer qtAvaliacoes;

	/*
	 * ações de login
	 */
	pointcut actionLogin(Login l):
		within(gui.Login)
		&&  call(private void logar()) && target(l);

	after(Login login):actionLogin(login){
		if (podeLogar(login.getMatricula(), login.getSenha())) {
			login(login.getMatricula(), login.getSenha(), login);
		}

	}

	private void login(String matricula, String pass, Login frame) {
		DaoProfessor dao = new DaoProfessor();
		String nome = dao.loginProfessor(matricula, pass);
		if (nome == null) {
			JOptionPane.showMessageDialog(null, "Matricula/Senha inválida!");
		} else {
			telaPrincipal = new TelaPrincipal(nome);
			telaProfessor = new TelaProfessor();
			telaPrincipal.setContent(telaProfessor.getContent());
			frame.dispose();
			this.matriculaProfessor = Integer.parseInt(matricula);
			telaPrincipal.setVisible(true);
		}
	}

	private boolean podeLogar(String matricula, String senha) {
		if (Assert.stringVazia(matricula)) {
			JOptionPane.showMessageDialog(null, "Campo \"Login\" está vazio",
					"Atenção", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (!Assert.apenasNumero(matricula)) {
			JOptionPane.showMessageDialog(null,
					"Campo \"Login\" deve ter somente números", "Atenção",
					JOptionPane.ERROR_MESSAGE);
			return false;

		} else {
			if (Assert.senhaVazia(senha)) {
				JOptionPane.showMessageDialog(null,
						"Campo \"Senha\" está vazio", "Atenção",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}
		return true;
	}

	/*
	 * ações tela princinpal
	 */
	pointcut fecharPrincipal(TelaPrincipal l):
		within(gui.TelaPrincipal)
		&& call(public void fecha()) && target(l);

	after(TelaPrincipal tela):fecharPrincipal(tela){
		tela.dispose();
	}

	pointcut voltar(TelaPrincipal l):
		within(gui.TelaPrincipal)
		&& call(private void voltarTelaProfessor()) && target(l);

	after(TelaPrincipal tela):voltar(tela){
		tela.setContent(telaProfessor.getContent());
	}

	pointcut actionTelaProfessorToTurmaAvaliacoes(TelaProfessor tp):
		within(gui.TelaProfessor)
		&&  call(private void telaTurmaAvaliacoes()) && target(tp);

	after(TelaProfessor tp):actionTelaProfessorToTurmaAvaliacoes(tp){
		janelaTurmaAvaliacoes();
	}

	pointcut actionTelaProfessorToTurmaNotasFaltas(TelaProfessor tp):
		within(gui.TelaProfessor)
		&&  call(private void telaTurmaNotasFaltas()) && target(tp);

	after(TelaProfessor tp):actionTelaProfessorToTurmaNotasFaltas(tp){
		janelaTurmaNotasFaltas();
	}

	pointcut actionTelaProfessorToTurmaGerarRelatorios(TelaProfessor tp):
		within(gui.TelaProfessor)
		&&  call(private void telaTurmaGerarRelatorios()) && target(tp);

	after(TelaProfessor tp):actionTelaProfessorToTurmaGerarRelatorios(tp){
		janelaTurmaGerarRelatorios();
	}

	private void janelaTurmaAvaliacoes() {
		DaoSql dta = new DaoSql();
		List<Serie> series = dta.getSeries(matriculaProfessor, false);
		List<Turma> turmas = dta.getTurmas(series.get(0).getNome(),
				matriculaProfessor, false);
		List<Materia> materias = dta.getMaterias(turmas.get(0).getCodigo(),
				matriculaProfessor, false);
		Integer qt_avaliacoes = dta.quantidadeAvaliacoesAtual(turmas.get(0)
				.getCodigo(), matriculaProfessor, materias.get(0).getCodigo());
		telaTurmaAvaliacoes = new TelaTurmaAvaliacoes(series, turmas, materias,
				qt_avaliacoes);
		telaPrincipal.setContent(telaTurmaAvaliacoes.getContent());
		ativarVoltar();
	}

	private void janelaTurmaNotasFaltas() {
		DaoSql dta = new DaoSql();
		List<Serie> series = dta.getSeries(matriculaProfessor, false);
		List<Turma> turmas = dta.getTurmas(series.get(0).getNome(),
				matriculaProfessor, false);
		List<Materia> materias = dta.getMaterias(turmas.get(0).getCodigo(),
				matriculaProfessor, false);
		Integer qt_avaliacoes = dta.quantidadeAvaliacoesAtual(turmas.get(0)
				.getCodigo(), matriculaProfessor, materias.get(0).getCodigo());
		telaTurmaCadernetas = new TelaTurmaCadernetas(series, turmas, materias,
				qt_avaliacoes);
		telaPrincipal.setContent(telaTurmaCadernetas.getContent());
		ativarVoltar();
	}

	private void janelaTurmaGerarRelatorios() {
		DaoSql dta = new DaoSql();
		List<Serie> series = dta.getSeries(matriculaProfessor, true);
		List<Turma> turmas = dta.getTurmas(series.get(0).getNome(),
				matriculaProfessor, true);
		List<Materia> materias = dta.getMaterias(turmas.get(0).getCodigo(),
				matriculaProfessor, true);
		Integer qt_avaliacoes = dta.quantidadeAvaliacoesAtual(turmas.get(0)
				.getCodigo(), matriculaProfessor, materias.get(0).getCodigo());
		relatorio = new Relatorio(series, turmas, materias, qt_avaliacoes);
		telaPrincipal.setContent(relatorio.getContent());
		ativarVoltar();
	}

	private void ativarVoltar() {
		telaPrincipal.ativarVoltar();
	}

	/*
	 * ações tela de turmaAvalições
	 */
	pointcut actionClassComponentComboBoxSerie(ClassComponent ta):
		within(gui.ClassComponent)
		&&  call(private void trocarTurmas()) && target(ta);

	after(ClassComponent ta):actionClassComponentComboBoxSerie(ta){
		try {
			DaoSql dta = new DaoSql();
			// Se estiver na tela de relatório, tem que trazer as encerradas
			// também
			boolean trazTodas = ta.getEstadoAtual() == Estado.TELA_RELATORIO;
			// System.out.println(ta.getEstadoAtual() ==
			// Estado.TELA_AVALIACOES);
			// System.out.println(ta.getEstadoAtual() ==
			// Estado.TELA_CADERNETAS);
			// System.out.println(ta.getEstadoAtual() == Estado.TELA_RELATORIO);
			// System.out
			// .println("---------------------------------------------------------");
			List<Turma> turmas = dta.getTurmas(ta.getSerieSelecionada()
					.getNome(), matriculaProfessor, trazTodas);
			List<Materia> materias = dta.getMaterias(turmas.get(0).getCodigo(),
					matriculaProfessor, trazTodas);
			qtAvaliacoes = dta.quantidadeAvaliacoesAtual(turmas.get(0)
					.getCodigo(), matriculaProfessor, materias.get(0)
					.getCodigo());
			ta.setElementosTurma(turmas);
			ta.setElementosMaterias(materias);
			if (ta.getEstadoAtual() == Estado.TELA_AVALIACOES) {
				if (qtAvaliacoes == null)
					qtAvaliacoes = 0;
				telaTurmaAvaliacoes.setNAvaliacoes(qtAvaliacoes);
			}
		} catch (Exception e) {

		}
	}

	pointcut actionClassComponentComboBoxTurma(ClassComponent ta):
		within(gui.ClassComponent)
		&&  call(private void trocarMaterias()) && target(ta);

	after(ClassComponent ta):actionClassComponentComboBoxTurma(ta){
		try {
			DaoSql dta = new DaoSql();
			boolean trazTodas = ta.getEstadoAtual() == Estado.TELA_RELATORIO;
			List<Materia> materias = dta.getMaterias(ta.getTurmaSelecionada()
					.getCodigo(), matriculaProfessor, trazTodas);
			qtAvaliacoes = dta.quantidadeAvaliacoesAtual(ta
					.getTurmaSelecionada().getCodigo(), matriculaProfessor,
					materias.get(0).getCodigo());
			ta.setElementosMaterias(materias);
			if (ta.getEstadoAtual() == Estado.TELA_AVALIACOES) {
				if (qtAvaliacoes == null)
					qtAvaliacoes = 0;
				telaTurmaAvaliacoes.setNAvaliacoes(qtAvaliacoes);
			}
		} catch (Exception e) {

		}
	}

	pointcut actionClassComponentComboBoxMateria(ClassComponent ta):
		within(gui.ClassComponent)
		&&  call(private void trocarNAvaliacoes()) && target(ta);

	after(ClassComponent ta):actionClassComponentComboBoxMateria(ta){
		try {
			DaoSql dta = new DaoSql();
			System.out.println(ta.getTurmaSelecionada().getCodigo());
			System.out.println(ta.getMateriaSelecionada().getCodigo());
			qtAvaliacoes = dta.quantidadeAvaliacoesAtual(ta
					.getTurmaSelecionada().getCodigo(), matriculaProfessor, ta
					.getMateriaSelecionada().getCodigo());

			if (ta.getEstadoAtual() == Estado.TELA_AVALIACOES) {
				if (qtAvaliacoes == null)
					qtAvaliacoes = 0;
				telaTurmaAvaliacoes.setNAvaliacoes(qtAvaliacoes);
			}
		} catch (Exception e) {

		}
	}

	pointcut actionTelaTurmaAvaliacoesDefinirQtNAvaliacoes(
			TelaTurmaAvaliacoes ta):
	 within(gui.TelaTurmaAvaliacoes)
	 && call(private void definirNAvaliacoes()) && target(ta);

	after(TelaTurmaAvaliacoes ta):actionTelaTurmaAvaliacoesDefinirQtNAvaliacoes(ta){
		if (Assert.apenasNumero(ta.getQtAvaliacoes())) {
			DaoSql dta = new DaoSql();
			boolean ok = dta.definirNAvaliacoes(ta.getTurmaSelecionada()
					.getCodigo(), matriculaProfessor, ta
					.getMateriaSelecionada().getCodigo(), Integer.valueOf(ta
					.getQtAvaliacoes()));
			if (ok) {
				JOptionPane.showMessageDialog(null,
						"Operação realizada com sucesso", "Atenção",
						JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane
						.showMessageDialog(
								null,
								"Operação não foi realizada com sucesso. Verifique arquivo de log!!",
								"Atenção", JOptionPane.ERROR_MESSAGE);
			}
		} else {
			JOptionPane
					.showMessageDialog(
							null,
							"Impossível definir quantidade de avaliações com texto. Digite somente números inteiros!!",
							"Atenção", JOptionPane.ERROR_MESSAGE);
		}

	}

	/*
	 * ações TelaTurmaCadernetas
	 */
	pointcut actionTelaProfessorToCaderneta(TelaTurmaCadernetas tc):
		within(gui.TelaTurmaCadernetas)
		&&  call(private void abrirCaderneta()) && target(tc);

	after(TelaTurmaCadernetas tc):actionTelaProfessorToCaderneta(tc){
		janelaCaderneta();
	}

	private void janelaCaderneta() {
		DaoSql d = new DaoSql();
		Materia m = telaTurmaCadernetas.getMateriaSelecionada();
		Turma t = telaTurmaCadernetas.getTurmaSelecionada();
		AlunoCadernetaNotas[] alunos = d.getDadosCaderneta(t, m,
				matriculaProfessor);
		caderneta = new Caderneta(telaPrincipal.getUserName(),
				telaTurmaCadernetas.getSerieSelecionada().getNome(),
				t.getNome(), m.getNome(), alunos);
		telaPrincipal.setContent(caderneta.getContent());
	}

	/*
	 * ações Caderneta
	 */
	pointcut actionCadernetaSalvarAlteracoes(Caderneta c):
		within(gui.Caderneta)
		&&  call(private void salvarAlteracoes()) && target(c);

	after(Caderneta c):actionCadernetaSalvarAlteracoes(c){
		salvarAlteracoes(c);
	}

	private void salvarAlteracoes(Caderneta c) {
		DaoSql d = new DaoSql();
		DefaultTableModel model = c.getTableModelNotas();
		AlunoCadernetaNotas[] dados = new AlunoCadernetaNotas[model
				.getRowCount()];
		for (int row = 0; row < model.getRowCount(); row++) {
			AlunoCadernetaNotas aluno = new AlunoCadernetaNotas();
			List<Double> notas = new ArrayList<Double>();
			aluno.setNotas(notas);
			for (int column = 0; column < model.getColumnCount(); column++) {
				Object valor = model.getValueAt(row, column);
				if (column == 0) {
					aluno.setMatricula((String) valor);
				} else if (column == 1) {
					aluno.setNome((String) valor);
				} else if (column == model.getColumnCount() - 1) {
					if (valor == null) {
						aluno.setMedia(null);
					} else if (valor instanceof Double) {
						aluno.setMedia((Double) valor);
					} else {
						aluno.setMedia(Double.parseDouble((String) valor));
					}
				} else {
					if (valor == null) {
						notas.add(null);
					} else if (valor instanceof Double) {
						notas.add((Double) valor);
					} else {
						notas.add(Double.parseDouble((String) valor));
					}
				}
			}
			dados[row] = aluno;
		}

		model = c.getTableModelFaltas();
		for (int row = 0; row < model.getRowCount(); row++) {
			AlunoCadernetaNotas aluno = dados[row];
			List<Boolean> faltas = new ArrayList<Boolean>();
			aluno.setFaltas(faltas);
			for (int column = 2; column < model.getColumnCount() - 1; column++) {
				faltas.add((Boolean) model.getValueAt(row, column));
			}
		}
		boolean ok = d
				.salvarAlteracoesCaderneta(dados,
						telaTurmaCadernetas.getTurmaSelecionada(),
						telaTurmaCadernetas.getMateriaSelecionada(),
						matriculaProfessor);
		if (ok) {
			JOptionPane.showMessageDialog(null,
					"Operação realizada com sucesso", "Atenção",
					JOptionPane.INFORMATION_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(null,
					"Operação não foi realizada com sucesso.", "Atenção",
					JOptionPane.ERROR_MESSAGE);
		}

	}

	pointcut actionCadernetaFecharCaderneta(Caderneta c):
		within(gui.Caderneta)
		&&  call(private void fecharCaderneta()) && target(c);

	after(Caderneta c):actionCadernetaFecharCaderneta(c){
		fecharCaderneta(c);
	}

	private void fecharCaderneta(Caderneta c) {
		DaoSql d = new DaoSql();
		int codigoCaderneta = d
				.getCodigoCaderneta(telaTurmaCadernetas.getTurmaSelecionada(),
						telaTurmaCadernetas.getMateriaSelecionada(),
						matriculaProfessor);
		if (!d.isNotasCadernetaPreenchidas(codigoCaderneta)) {
			JOptionPane
					.showMessageDialog(null,
							"Nem todas as notas estão preenchidas, não foi possível fechar a caderneta!");
			return;
		}
		d.fecharCaderneta(codigoCaderneta);
		JOptionPane.showMessageDialog(null, "Caderneta fechada com sucesso!");
	}

	/*
	 * ações da tela de relatorio
	 */
	pointcut acaoGeraRelatorioFinal(Relatorio r):
			within(gui.Relatorio)
			&& call(public void geraRelatorioFinal()) && target(r);

	after(Relatorio r):acaoGeraRelatorioFinal(r){
		DaoReport report = new DaoReport();
		List<RelatorioFinalBean> dados = report.geraRelatorioFinal(
				r.getTurmaSelecionada(), r.getMateriaSelecionada(),
				r.getSerieSelecionada());
		JRDataSource dataSource = new JRBeanCollectionDataSource(dados);
		try {
			if (!(new File("report/RelatorioFinal.jasper").exists())) {
				JasperCompileManager.compileReportToFile(
						"report/RelatorioFinal.jrxml",
						"report/RelatorioFinal.jasper");
			}
			byte[] pdf = JasperRunManager.runReportToPdf(
					"report/RelatorioFinal.jasper", null, dataSource);
			if (pdf != null && pdf.length > 0) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileFilter(new FileFilter() {
					@Override
					public String getDescription() {
						return "PDF Files (*.pdf)";
					}

					@Override
					public boolean accept(File f) {
						return f.getPath().endsWith(".pdf");
					}
				});

				if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					if (!file.getPath().endsWith(".pdf")) {
						file = new File(file.getPath() + ".pdf");
					}
					FileOutputStream os = new FileOutputStream(file);
					os.write(pdf);
					os.close();
					JOptionPane.showMessageDialog(null,
							"Relatório salvo com sucesso!");
				}

			}
		} catch (JRException e) {
			JOptionPane.showMessageDialog(null, "Erro ao gerar o relatório");
			e.printStackTrace();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Erro ao salvar o relatório");
			e.printStackTrace();
		}

	}

	pointcut acaoGeraRelatorioDiarioDeClasse(Relatorio r):
			within(gui.Relatorio)
			&& call(public void geraRelatorioDiarioDeClasse()) && target(r);

	after(Relatorio r):acaoGeraRelatorioDiarioDeClasse(r){
		try {
			if (!(new File("report/DiarioClasse.jasper").exists())) {
				JasperCompileManager.compileReportToFile(
						"report/DiarioClasse.jrxml",
						"report/DiarioClasse.jasper");
			}
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("codigoTurma", r.getTurmaSelecionada().getCodigo());
			params.put("codigoSerie", r.getSerieSelecionada().getCodigo());
			params.put("codigoDisciplina", r.getMateriaSelecionada()
					.getCodigo());

			Connection con = Connect.abreConexao();
			byte[] pdf = JasperRunManager.runReportToPdf(
					"report/DiarioClasse.jasper", params, con);
			con.close();
			if (pdf != null && pdf.length > 0) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileFilter(new FileFilter() {
					@Override
					public String getDescription() {
						return "PDF Files (*.pdf)";
					}

					@Override
					public boolean accept(File f) {
						return f.getPath().endsWith(".pdf");
					}
				});

				if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					if (!file.getPath().endsWith(".pdf")) {
						file = new File(file.getPath() + ".pdf");
					}
					FileOutputStream os = new FileOutputStream(file);
					os.write(pdf);
					os.close();
					JOptionPane.showMessageDialog(null,
							"Relatório salvo com sucesso!");
				}
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao gerar o relatório");
			e.printStackTrace();
		} catch (JRException e) {
			JOptionPane.showMessageDialog(null, "Erro ao gerar o relatório");
			e.printStackTrace();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Erro ao salvar o relatório");
			e.printStackTrace();
		}
	}
}
