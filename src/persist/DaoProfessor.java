package persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//classe de manipulação do professor
public class DaoProfessor {

	// Metodo de login para o sistema
	public String loginProfessor(String matricula, String pass) {
		Connection con = null;
		try {
			con = Connect.abreConexao();
			PreparedStatement stm = con
					.prepareStatement("SELECT * FROM tb_professor WHERE matricula = ? AND senha = ?");
			stm.setInt(1, Integer.parseInt(matricula));
			stm.setString(2, pass);
			ResultSet rs = stm.executeQuery();
			if (rs.next()) {
				return rs.getString("nome");
			} else {
				return null;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return null;
	}

	public static void main(String[] args) {
		DaoProfessor d = new DaoProfessor();
		d.loginProfessor("2", "ss");
	}

	public String loginProfessor() {
		return "ana";
	}

}
