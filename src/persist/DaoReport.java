package persist;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entidade.Materia;
import entidade.RelatorioFinalBean;
import entidade.Serie;
import entidade.Turma;

public class DaoReport {
	public List<RelatorioFinalBean> geraRelatorioFinal(Turma turma, Materia materia, Serie serie) {
		String sql = 
			"SELECT r.codigo AS codigo_resultados, c.isencerrada AS statusDisciplina, m.codigo AS codigoDisciplina, " +
			"t.nome AS nomeTurma, p.nome AS nomeProfessor, m.nome AS nomeDisciplina, " +
			"a.matricula, a.nome AS nomeAluno,  " +
			"(SELECT COUNT(codigo) FROM tb_presenca WHERE ispresente = false and codigo_resultados = r.codigo) AS faltas, " +
			"(SELECT COUNT(codigo) FROM tb_presenca WHERE codigo_resultados = r.codigo) AS totalAulas, " +
			"r.situacao AS situacaoFinal, (SELECT COUNT(codigo) FROM tb_nota WHERE codigo_resultados = r.codigo GROUP BY codigo_resultados) AS totalProvas " +
			
			"FROM tb_caderneta c " + 
			
			"INNER JOIN tb_resultados r ON (r.codigo_caderneta = c.codigo) " + 
			"INNER JOIN tb_materia m ON (m.codigo = c.codigo_materia)  " +
			"INNER JOIN tb_turma t ON (t.codigo = c.codigo_turma)  " +
			"INNER JOIN tb_serie s ON (s.codigo = t.codigo_serie)  " +
			"INNER JOIN tb_professor p ON (c.matricula_professor = p.matricula) " + 
			"INNER JOIN tb_aluno a ON (a.matricula = r.matricula_aluno)   " +
			"WHERE m.codigo = ? AND t.codigo = ? AND s.codigo = ? " +
			"ORDER BY a.nome";
		Connection con = Connect.abreConexao();
		List<RelatorioFinalBean> dados = new ArrayList<RelatorioFinalBean>();
		
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, materia.getCodigo());
			ps.setInt(2, turma.getCodigo());
			ps.setInt(3, serie.getCodigo());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				RelatorioFinalBean bean = new RelatorioFinalBean();
				bean.setCodigoDisciplina(rs.getInt("codigoDisciplina"));
				bean.setFaltas(rs.getInt("faltas"));
				bean.setMatricula(rs.getInt("matricula"));
				bean.setNomeAluno(rs.getString("nomeAluno"));
				bean.setNomeDisciplina(rs.getString("nomeDisciplina"));
				bean.setNomeProfessor(rs.getString("nomeProfessor"));
				bean.setNomeTurma(rs.getString("nomeTurma"));
				bean.setSituacaoFinal(rs.getString("situacaoFinal"));
				bean.setStatusDisciplina(rs.getBoolean("statusDisciplina"));
				bean.setTotalProvas(rs.getInt("totalProvas"));
				int totalAulas = rs.getInt("totalAulas");
				int faltas = rs.getInt("faltas");
				bean.setFrequencia(((totalAulas - faltas) / (double) totalAulas) * 100);
				
				PreparedStatement ps2 = con.prepareStatement("SELECT valor FROM tb_nota WHERE codigo_resultados = ? ORDER BY codigo");
				ps2.setInt(1, rs.getInt("codigo_resultados"));
				ResultSet rs2 = ps2.executeQuery();
				int i = 0;
				Double media = 0d;
				while (rs2.next()) {
					BigDecimal valor = (BigDecimal) rs2.getObject("valor");
					switch (i) {
					case 0:
						bean.setMedia1(valor == null ? null : valor.doubleValue());
						media += bean.getMedia1() == null ? 0 : bean.getMedia1();
						break;
					case 1:
						bean.setMedia2(valor == null ? null : valor.doubleValue());
						media += bean.getMedia2() == null ? 0 : bean.getMedia2();
						break;
					case 2:
						bean.setMedia3(valor == null ? null : valor.doubleValue());
						media += bean.getMedia3() == null ? 0 : bean.getMedia3();
						break;
					case 3:
						bean.setMedia4(valor == null ? null : valor.doubleValue());
						media += bean.getMedia4() == null ? 0 : bean.getMedia4();
						break;
					case 4:
						bean.setMedia5(valor == null ? null : valor.doubleValue());
						media += bean.getMedia5() == null ? 0 : bean.getMedia5();
						break;
					}
					++i;
				}
				rs2.close();
				ps2.close();
				
				bean.setMediaFinal(media /= bean.getTotalProvas());
				dados.add(bean);
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		
		return dados;
	}
}
