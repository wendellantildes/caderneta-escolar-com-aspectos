package persist;

import persist.Connect;
import entidade.Aluno;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

//Classe referente a persistencia de alunos.
public class DaoAluno {
	private Connection con;
	
	//Abre a conexao com o banco de dados
	public DaoAluno(){
		Connect abre = new Connect();
		con = abre.abreConexao();
		
	}
	
	//Salva aluno na tabela tb_aluno do banco
	public void insertAluno(Aluno a){
		String sql = "INSERT INTO tb_aluno";
		try{
		
			PreparedStatement stm = con.prepareStatement(sql);
			stm.setDouble(1, a.getMatricula());
			stm.setString(2, a.getNome());
			stm.execute();
			stm.close();
			con.close();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	//Apaga um aluno do banco de dados
	public void deleteAluno(double matricula){
		String sql = "DELETE tb_aluno WHERE matricula='"+matricula+"'";
		try{
		
			Statement stm = con.createStatement();
			stm.execute(sql);
			stm.close();
			con.close();
			
		}catch(SQLException a){
			a.printStackTrace();
		}
	}
	
	//Atualiza um aluno do banco de dados
	public void updateAluno(Aluno a){
		
	}
	
	//Faz uma busca no banco em busca do nome do parametro
	public List<Aluno> selectAluno(String nome){
		List<Aluno> result = new ArrayList<Aluno>();
		
		
		
		return result;
	}

}
