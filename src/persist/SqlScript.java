package persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlScript {
	public static void main(String[] args) {
		Connection con = Connect.abreConexao();
		try {
			con.setAutoCommit(false);
			PreparedStatement psInsert = con.prepareStatement("INSERT INTO tb_presenca (codigo_resultados) VALUES (?)");
			PreparedStatement ps = con.prepareStatement("SELECT r.codigo, m.qt_creditos FROM tb_caderneta c " +
					"INNER JOIN tb_resultados r ON (c.codigo = r.codigo_caderneta) " +
					"INNER JOIN tb_materia m ON (m.codigo = c.codigo_materia)");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int qtCreditos = rs.getInt("qt_creditos");
				psInsert.setInt(1, rs.getInt("codigo"));
				for (int i = 0; i < (qtCreditos * 15) / 2; i++) {
					psInsert.executeUpdate();
				}
			}
			rs.close();
			ps.close();
			psInsert.close();
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}
}
