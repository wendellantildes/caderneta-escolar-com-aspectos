package persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelo.AlunoCadernetaNotas;
import entidade.Materia;
import entidade.Serie;
import entidade.Turma;

public class DaoSql {
	public List<Serie> getSeries(Integer matriculaProfessor, boolean todas) {
		Connection con = null;
		try {
			con = Connect.abreConexao();
			String sql = "SELECT s.codigo,s.nome FROM tb_caderneta c inner join tb_turma t on "
				+ "(c.codigo_turma = t.codigo) inner join tb_serie s on (t.codigo_serie = s.codigo)"
				+ "WHERE c.matricula_professor = ?";
			if (!todas) {
				sql +=  " and c.isencerrada = false";
			}
			PreparedStatement stm = con
					.prepareStatement(sql);
			stm.setInt(1, matriculaProfessor);
			ResultSet rs = stm.executeQuery();
			List<Serie> series = new ArrayList<Serie>();
			while (rs.next()) {
				Serie s = new Serie();
				s.setCodigo(rs.getInt("codigo"));
				s.setNome(rs.getString("nome"));
				System.out.println(s.getNome() + s.getCodigo());
				series.add(s);
			}
			if (series.size() != 0) {
				return series;
			} else {
				return null;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return null;
	}

	public List<Turma> getTurmas(String serie, Integer matriculaProfessor, boolean todas) {
		Connection con = null;
		try {
			con = Connect.abreConexao();
			String sql = "SELECT t.codigo,t.nome FROM tb_caderneta c inner join tb_turma t on "
				+ "(c.codigo_turma = t.codigo) inner join tb_serie s on (s.nome = ?)"
				+ "WHERE c.matricula_professor = ? and s.codigo = t.codigo_serie";
			if (!todas) {
				sql += " and c.isencerrada = false";
			}
			PreparedStatement stm = con
					.prepareStatement(sql);
			stm.setString(1, serie);
			stm.setInt(2, matriculaProfessor);
			ResultSet rs = stm.executeQuery();
			List<Turma> turmas = new ArrayList<Turma>();
			while (rs.next()) {
				Turma t = new Turma();
				t.setCodigo(rs.getInt("codigo"));
				t.setNome(rs.getString("nome"));
				System.out.println(rs.getString("nome"));
				turmas.add(t);
			}
			if (turmas.size() != 0) {
				return turmas;
			} else {
				return null;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return null;
	}

	public List<Materia> getMaterias(Integer codigo_turma,
			Integer matriculaProfessor, boolean todas) {
		Connection con = null;
		try {
			con = Connect.abreConexao();
			String sql = "SELECT m.codigo,m.nome FROM tb_caderneta c inner join tb_materia m on "
				+ "(c.codigo_materia = m.codigo) "
				+ "WHERE c.matricula_professor = ? and  c.codigo_turma = ?";
			if (!todas) {
				sql += " and c.isencerrada = false";
			}
			PreparedStatement stm = con.prepareStatement(sql);
			stm.setInt(1, matriculaProfessor);
			stm.setInt(2, codigo_turma);
			ResultSet rs = stm.executeQuery();
			List<Materia> materias = new ArrayList<Materia>();
			while (rs.next()) {
				Materia m = new Materia();
				m.setCodigo(rs.getInt("codigo"));
				m.setNome(rs.getString("nome"));
				System.out.println(rs.getString("nome"));
				materias.add(m);
			}
			if (materias.size() != 0) {
				return materias;
			} else {
				return null;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return null;
	}

	public Integer quantidadeAvaliacoesAtual(Integer codigo_turma,
			Integer matriculaProfessor, Integer codigo_materia) {
		Connection con = null;
		try {
			con = Connect.abreConexao();
			PreparedStatement stm = con
					.prepareStatement("SELECT count(n.codigo_resultados) as notas FROM tb_caderneta c inner join tb_resultados r on "
							+ "(c.codigo = r.codigo_caderneta) inner join tb_nota n on (n.codigo_resultados = r.codigo)"
							+ "WHERE c.matricula_professor = ? and  c.codigo_turma = ? and c.codigo_materia = ? and c.isencerrada = false GROUP BY n.codigo_resultados");
			stm.setInt(1, matriculaProfessor);
			stm.setInt(2, codigo_turma);
			stm.setInt(3, codigo_materia);
			ResultSet rs = stm.executeQuery();
			while (rs.next()) {
				System.out.println(rs.getInt("notas"));
				return rs.getInt("notas");

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return null;
	}

	public boolean definirNAvaliacoes(Integer codigo_turma,
			Integer matriculaProfessor, Integer codigo_materia,
			Integer qt_avaliacoes) {
		Connection con = null;
		try {
			con = Connect.abreConexao();
			con.setAutoCommit(false);
			PreparedStatement stm = con
					.prepareStatement("SELECT r.codigo FROM tb_caderneta c inner join tb_resultados r on "
							+ "(c.codigo = r.codigo_caderneta)"
							+ "WHERE c.matricula_professor = ? and  c.codigo_turma = ? and c.codigo_materia = ?");
			stm.setInt(1, matriculaProfessor);
			stm.setInt(2, codigo_turma);
			stm.setInt(3, codigo_materia);
			ResultSet rs = stm.executeQuery();
			while (rs.next()) {
				PreparedStatement ins = con
						.prepareStatement("Insert INTO tb_nota(valor,codigo_resultados) "
								+ "values(null,?)");
				for (int i = 0; i < qt_avaliacoes; i++) {
					ins.setInt(1, rs.getInt("codigo"));
					ins.execute();
				}

			}
			con.commit();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			return false;
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}
	
	public Integer getCodigoCaderneta(Turma turma, Materia materia, Integer matriculaProfessor) {
		Connection con = Connect.abreConexao();
		Integer codigo = null;
		try {
			PreparedStatement ps = con.prepareStatement("SELECT c.codigo " +
													    "FROM tb_caderneta c " + 
													    "WHERE c.matricula_professor = ? and  c.codigo_turma = ? and c.codigo_materia = ?");
			ps.setInt(1, matriculaProfessor);
			ps.setInt(2, turma.getCodigo());
			ps.setInt(3, materia.getCodigo());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				codigo = rs.getInt("codigo");
			}
			rs.close();
			ps.close();
			return codigo;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return codigo; 
	}
	public Integer getCodigoResultados(Turma turma, Materia materia, Integer matriculaProfessor, Integer matriculaAluno) {
		Connection con = Connect.abreConexao();
		Integer codigo = null;
		try {
			PreparedStatement ps = con.prepareStatement("SELECT r.codigo " +
													    "FROM tb_caderneta c INNER JOIN tb_resultados r ON (r.codigo_caderneta = c.codigo)" + 
													    "WHERE c.matricula_professor = ? and  c.codigo_turma = ? and c.codigo_materia = ? " +
													    "and r.matricula_aluno = ?");
			ps.setInt(1, matriculaProfessor);
			ps.setInt(2, turma.getCodigo());
			ps.setInt(3, materia.getCodigo());
			ps.setInt(4, matriculaAluno);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				codigo = rs.getInt("codigo");
			}
			rs.close();
			ps.close();
			return codigo;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return codigo; 
	}
	public AlunoCadernetaNotas[] getDadosCaderneta(Turma turma, Materia materia, Integer matriculaProfessor) {
		Connection con = Connect.abreConexao();
		try {
			PreparedStatement ps = con.prepareStatement("SELECT COUNT(r.matricula_aluno) AS totalAlunos " +
													    "FROM tb_resultados r " + 
													    "WHERE r.codigo_caderneta = ?");
			int codigoCaderneta = getCodigoCaderneta(turma, materia, matriculaProfessor);
			ps.setInt(1, codigoCaderneta);
			ResultSet rs = ps.executeQuery();
			rs.next();
			int totalAlunos = rs.getInt("totalAlunos");
			rs.close();
			ps.close();
			
			AlunoCadernetaNotas[] dados = new AlunoCadernetaNotas[totalAlunos];
			ps = con.prepareStatement("SELECT a.nome, a.matricula, r.codigo " +
				"FROM tb_resultados r " +
				"INNER JOIN tb_aluno a ON (r.matricula_aluno = a.matricula) " +
				"WHERE r.codigo_caderneta = ? " +
				"ORDER BY a.nome");
			ps.setInt(1, codigoCaderneta);
			rs = ps.executeQuery();
			int i = 0;
			while (rs.next()) {
				AlunoCadernetaNotas aluno = new AlunoCadernetaNotas();
				aluno.setMatricula(rs.getString("matricula"));
				aluno.setNome(rs.getString("nome"));
				PreparedStatement ps2 = con.prepareStatement("SELECT valor FROM tb_nota WHERE codigo_resultados = ? ORDER BY codigo");
				int codigoResultado = rs.getInt("codigo");
				ps2.setInt(1, codigoResultado);
				ResultSet rs2 = ps2.executeQuery();
				List<Double> notas = new ArrayList<Double>();
				Double media = 0d;
				int naoNulos = 0;
				while (rs2.next()) {
					String s = rs2.getString("valor");
					Double nota = s == null ? null : Double.parseDouble(s);
					notas.add(nota);
					if (nota != null) {
						media += nota;
						naoNulos++;
					}
				}
				rs2.close();
				ps2.close();
				
				media /= naoNulos;
				aluno.setNotas(notas);
				aluno.setMedia(media);
				
				ps2 = con.prepareStatement("SELECT ispresente FROM tb_presenca WHERE codigo_resultados = ? ORDER BY codigo");
				ps2.setInt(1, codigoResultado);
				rs2 = ps2.executeQuery();
				List<Boolean> faltas = new ArrayList<Boolean>();
				while (rs2.next()) {
					faltas.add(rs2.getBoolean("ispresente"));
				}
				rs2.close();
				ps2.close();
				
				aluno.setFaltas(faltas);
				dados[i++] = aluno;
			}
			return dados;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean salvarAlteracoesCaderneta(AlunoCadernetaNotas[] dados, Turma turma, Materia materia, Integer matriculaProfessor) {
		Connection con = Connect.abreConexao();
		boolean ok;
		try {
			PreparedStatement ps = con.prepareStatement("UPDATE tb_nota SET valor = ? WHERE codigo = ?");
			PreparedStatement ps4 = con.prepareStatement("UPDATE tb_presenca SET ispresente = ? WHERE codigo = ?");
			for (AlunoCadernetaNotas aluno : dados) {
				List<Double> notas = aluno.getNotas();
				PreparedStatement ps2 = con.prepareStatement("SELECT codigo FROM tb_nota WHERE codigo_resultados = ? ORDER BY codigo");
				int codigoResultados = getCodigoResultados(turma, materia, matriculaProfessor, Integer.parseInt(aluno.getMatricula()));
				ps2.setInt(1, codigoResultados);
				ResultSet rs = ps2.executeQuery();
				for (Double nota : notas) {
					ps.setObject(1, nota);
					rs.next();
					ps.setInt(2, rs.getInt("codigo"));
					ps.executeUpdate();
				}
				
				List<Boolean> faltas = aluno.getFaltas();
				PreparedStatement ps3 = con.prepareStatement("SELECT codigo FROM tb_presenca WHERE codigo_resultados = ?");
				ps3.setInt(1, codigoResultados);
				ResultSet rs2 = ps3.executeQuery();
				for (Boolean falta : faltas) {
					rs2.next();
					ps4.setObject(1, falta);
					ps4.setInt(2, rs2.getInt("codigo"));
					ps4.executeUpdate();
				}
			}
			ps.close();
			ok = true;
		} catch (SQLException e) {
			e.printStackTrace();
			ok = false;
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					ok = false;
				}
		}
		return ok;
	}
	
	public boolean isNotasCadernetaPreenchidas(Integer codigoCaderneta) {
		Connection con = Connect.abreConexao();
		try {
			PreparedStatement ps = con.prepareStatement("SELECT valor FROM tb_nota WHERE codigo_resultados = ? AND valor IS NULL");
			PreparedStatement ps2 = con.prepareStatement("SELECT r.codigo FROM tb_caderneta c INNER JOIN tb_resultados r ON (r.codigo_caderneta = c.codigo) WHERE c.codigo = ?");
			ps2.setInt(1, codigoCaderneta);
			ResultSet rs2 = ps2.executeQuery();
			boolean ok = true;
			while (rs2.next()) {
				ps.setInt(1, rs2.getInt("codigo"));
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					ok = false;
					break;
				}
			}
			rs2.close();
			ps2.close();
			ps.close();
			return ok;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return false;
	}
	
	public void fecharCaderneta(Integer codigoCaderneta) {
		Connection con = Connect.abreConexao();
		try {
			con.setAutoCommit(false);
			PreparedStatement ps = con.prepareStatement("UPDATE tb_caderneta SET isencerrada = true WHERE codigo = ?");
			ps.setInt(1, codigoCaderneta);
			ps.executeUpdate();
			ps.close();
			
			ps = con.prepareStatement("UPDATE tb_resultados SET situacao = ? WHERE codigo = ?");
			PreparedStatement ps2 = con.prepareStatement("SELECT codigo FROM tb_resultados WHERE codigo_caderneta = ?");
			ps2.setInt(1, codigoCaderneta);
			ResultSet rs2 = ps2.executeQuery();
			while (rs2.next()) {
				PreparedStatement ps3 = con.prepareStatement("SELECT valor FROM tb_nota WHERE codigo_resultados = ?");
				PreparedStatement ps4 = con.prepareStatement("SELECT ispresente FROM tb_presenca WHERE codigo_resultados = ?");
				int codigoResultado = rs2.getInt("codigo");
				ps3.setInt(1, codigoResultado);
				
				
				ResultSet rs3 = ps3.executeQuery();
				double media = 0d;
				int totalProvas = 0;
				while (rs3.next()) {
					totalProvas++;
					media += rs3.getDouble("valor");
				}
				rs3.close();
				ps3.close();
				media /= totalProvas;
				
				if (media < 6) {
					ps.setString(1, "r");
					ps.setInt(2, codigoResultado);
					ps.executeUpdate();
					continue;
				}
				
				ps4.setInt(1, codigoResultado);
				ResultSet rs4 = ps4.executeQuery();
				int totalAulas = 0;
				int faltas = 0;
				double frequencia = 0d;
				while (rs4.next()) {
					totalAulas++;
					if (!rs4.getBoolean("ispresente")) {
						faltas++;
					}
				}
				rs4.close();
				ps4.close();
				frequencia = ((totalAulas - faltas) / totalAulas) * 100;
				if (frequencia < 75) {
					ps.setString(1, "r");
					ps.setInt(2, codigoResultado);
					ps.executeUpdate();
					continue;
				}
				
				ps.setString(1, "a");
				ps.setInt(2, codigoResultado);
				ps.executeUpdate();
			}
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
}
