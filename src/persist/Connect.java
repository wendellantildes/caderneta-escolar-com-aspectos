package persist;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;


// Classe de conexao com o banco de dados
//Classe de conexao com o banco de dados
public final class Connect {
	
	//Metodo que abre uma conexao com o banco da dados postgres e retorna essa conexao.
	public static Connection abreConexao(){
		try {
			Class.forName("org.postgresql.Driver");
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/poa", "postgres", "postgres");
			return con;
		} catch(SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}
