package entidade;

import java.util.List;

public class Caderneta {
	private Turma turma;
	private Professor professor;
	private Materia materia;
	private List<Resultados> resultados;
	private DiarioDeClasse diario;
	private Boolean isEncerrada;

	public List<Resultados> getResultados() {
		return resultados;
	}

	public void setResultados(List<Resultados> resultados) {
		this.resultados = resultados;
	}

	public DiarioDeClasse getDiario() {
		return diario;
	}

	public void setDiario(DiarioDeClasse diario) {
		this.diario = diario;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}

	public Boolean isEncerrada() {
		return isEncerrada;
	}

	public void setIsEncerrada(Boolean isEncerrada) {
		this.isEncerrada = isEncerrada;
	}	

}
