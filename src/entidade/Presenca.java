package entidade;

public class Presenca {
	private Aluno aluno;
	private Aula aula;
	private Boolean isPresente;

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public Aula getAula() {
		return aula;
	}

	public void setAula(Aula aula) {
		this.aula = aula;
	}

	public Boolean getIsPresente() {
		return isPresente;
	}

	public void setIsPresente(Boolean isPresente) {
		this.isPresente = isPresente;
	}
}
