package entidade;

public class RelatorioFinalBean {

	private String nomeProfessor;
	private Integer codigoDisciplina;
	private String nomeDisciplina;
	private Boolean statusDisciplina;
	private Integer matricula;
	private String nomeAluno;
	private Double media1 = null;
	private Double media2 = null;
	private Double media3 = null;
	private Double media4 = null;
	private Double media5 = null;
	private Double mediaFinal;
	private Double frequencia;
	private Integer faltas;
	private String situacaoFinal;
	private Integer totalProvas;
	private String nomeTurma;
	
	public String getNomeProfessor() {
		return nomeProfessor;
	}
	public void setNomeProfessor(String nomeProfessor) {
		this.nomeProfessor = nomeProfessor;
	}
	public Integer getCodigoDisciplina() {
		return codigoDisciplina;
	}
	public void setCodigoDisciplina(Integer codigoDisciplina) {
		this.codigoDisciplina = codigoDisciplina;
	}
	public String getNomeDisciplina() {
		return nomeDisciplina;
	}
	public void setNomeDisciplina(String nomeDisciplina) {
		this.nomeDisciplina = nomeDisciplina;
	}
	public Boolean getStatusDisciplina() {
		return statusDisciplina;
	}
	public void setStatusDisciplina(Boolean statusDisciplina) {
		this.statusDisciplina = statusDisciplina;
	}
	public Integer getMatricula() {
		return matricula;
	}
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
	public String getNomeAluno() {
		return nomeAluno;
	}
	public void setNomeAluno(String nomeAluno) {
		this.nomeAluno = nomeAluno;
	}
	public Double getMedia1() {
		return media1;
	}
	public void setMedia1(Double media1) {
		this.media1 = media1;
	}
	public Double getMedia2() {
		return media2;
	}
	public void setMedia2(Double media2) {
		this.media2 = media2;
	}
	public Double getMedia3() {
		return media3;
	}
	public void setMedia3(Double media3) {
		this.media3 = media3;
	}
	public Double getMedia4() {
		return media4;
	}
	public void setMedia4(Double media4) {
		this.media4 = media4;
	}
	public Double getMedia5() {
		return media5;
	}
	public void setMedia5(Double media5) {
		this.media5 = media5;
	}
	public Double getMediaFinal() {
		return mediaFinal;
	}
	public void setMediaFinal(Double mediaFinal) {
		this.mediaFinal = mediaFinal;
	}
	public Double getFrequencia() {
		return frequencia;
	}
	public void setFrequencia(Double frequencia) {
		this.frequencia = frequencia;
	}
	public Integer getFaltas() {
		return faltas;
	}
	public void setFaltas(Integer faltas) {
		this.faltas = faltas;
	}
	public String getSituacaoFinal() {
		return situacaoFinal;
	}
	public void setSituacaoFinal(String situacaoFinal) {
		this.situacaoFinal = situacaoFinal;
	}
	public Integer getTotalProvas() {
		return totalProvas;
	}
	public void setTotalProvas(Integer totalProvas) {
		this.totalProvas = totalProvas;
	}
	public String getNomeTurma() {
		return nomeTurma;
	}
	public void setNomeTurma(String nomeTurma) {
		this.nomeTurma = nomeTurma;
	}
}
