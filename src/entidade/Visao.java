package entidade;

public class Visao {
	private Aluno aluno;
	private DiarioDeClasse diario;
	private String visao;

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public DiarioDeClasse getDiario() {
		return diario;
	}

	public void setDiario(DiarioDeClasse diario) {
		this.diario = diario;
	}

	public String getVisao() {
		return visao;
	}

	public void setVisao(String visao) {
		this.visao = visao;
	}
}
