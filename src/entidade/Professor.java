package entidade;

import java.util.List;

public class Professor {
	private String nome;
	private String matricula;
	private String senha;
	private List<Materia> materiaQPodeLecionar;
	
	
	public void setSenha(String senha){
		this.senha = senha;
	}
	
	public String getSenha(){
		return this.senha;
	}
	
	public void setMatricula(String matricula){
		this.matricula = matricula;
	}
	
	public String getMatricula(){
		return this.matricula;
	}
	
	public List<Materia> getMateriaQPodeLecionar() {
		return materiaQPodeLecionar;
	}

	public void setMateriaQPodeLecionar(List<Materia> materiaQPodeLecionar) {
		this.materiaQPodeLecionar = materiaQPodeLecionar;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
