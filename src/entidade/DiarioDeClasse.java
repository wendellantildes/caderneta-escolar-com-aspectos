package entidade;

import java.util.List;

public class DiarioDeClasse {
	private List<Aula> aulas;

	public List<Aula> getAulas() {
		return aulas;
	}

	public void setAulas(List<Aula> aulas) {
		this.aulas = aulas;
	}
}
