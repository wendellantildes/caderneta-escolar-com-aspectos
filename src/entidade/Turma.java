package entidade;

import java.util.List;

public class Turma {
	private Integer codigo;
	private String nome;	
	private Integer qtAvaliacoes;
	private Serie serie;
	private List<Aluno> alunos;
	private boolean situacao;
	private Integer aulas;
	
	public void setAulas(Integer aulas){
		this.aulas = aulas;
	}
	
	public Integer getAulas(){
		return this.aulas;
	}
	
	public void setSituacao(boolean situacao){
		this.situacao = situacao;
	}
	
	public boolean getSituacao(){
		return this.situacao;
	}
	
	public Integer getQtAvaliacoes() {
		return qtAvaliacoes;
	}

	
	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	public void setQtAvaliacoes(Integer qtAvaliacoes) {
		this.qtAvaliacoes = qtAvaliacoes;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Serie getSerie() {
		return serie;
	}

	public void setSerie(Serie serie) {
		this.serie = serie;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Integer getCodigo() {
		return codigo;
	}
	public String toString(){
		return nome;
	}

}
