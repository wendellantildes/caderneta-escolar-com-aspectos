﻿CREATE TABLE tb_serie (
	codigo  serial primary key,
	nome varchar(20) not null
);

CREATE TABLE tb_turma(
	codigo  serial primary key,
	nome    varchar(20) not null,
	codigo_serie   integer not null references tb_serie(codigo)
);
CREATE TABLE tb_aluno(
	matricula  serial primary key,
	nome varchar(50) not null
);

CREATE TABLE tb_turma_aluno(
	codigo_turma integer references tb_turma(codigo),
	matricula_aluno integer references tb_aluno(matricula),
	PRIMARY KEY(codigo_turma,matricula_aluno)
);
CREATE TABLE tb_materia(
	codigo serial primary key,
	nome varchar(20) not null
);
CREATE TABLE tb_ementa(
	codigo serial primary key,
	descricao text not null,
	codigo_serie integer not null references tb_serie(codigo),
	codigo_materia integer not null references tb_materia(codigo)
);
CREATE TABLE tb_professor(
	matricula serial primary key,
	nome varchar(50) not null,
	senha char(32) not null
);
CREATE TABLE tb_professor_materia(
	matricula_professor integer not null references tb_professor(matricula),
	codigo_materia integer not null references tb_materia(codigo),
	PRIMARY KEY(matricula_professor,codigo_materia)
);
CREATE TABLE tb_caderneta(
	codigo serial primary key,
	matricula_professor integer references tb_professor(matricula),
	codigo_materia integer not null references tb_materia(codigo),
	codigo_turma integer not null references  tb_turma(codigo),
	isEncerrada boolean not null
);
CREATE TABLE tb_resultados(
	codigo serial primary key,
	media decimal not null,
	frequencia decimal not null,
	situacao char(1) not null check(situacao = 'a' 
			or situacao = 'r' or situacao = 'i'),
	codigo_caderneta integer not null references tb_caderneta(codigo),
	matricula_aluno integer not null references tb_aluno(matricula)
);
CREATE TABLE tb_nota(
	codigo serial primary key,
	valor decimal not null,
	codigo_resultados integer not null references tb_resultados(codigo)
);
CREATE TABLE tb_diario_de_classe(
	codigo serial primary key,
	descricao text not null,
	codigo_caderneta integer not null references tb_caderneta(codigo)
);
CREATE TABLE tb_aula(
	codigo serial primary key,
	resumo text not null,
	codigo_diario_de_classe integer not null references tb_diario_de_classe(codigo)
);
CREATE TABLE tb_presenca(
	codigo serial primary key,
	isPresente boolean not null,
	matricula_aluno integer not null references tb_aluno(matricula),
	codigo_aula integer not null references tb_aula(codigo)
);
CREATE TABLE tb_visao(	
	codigo serial primary key,
	visao text, 
	matricula_aluno integer not null references tb_aluno(matricula),
	codigo_diario_de_classe integer not null references tb_diario_de_classe(codigo)
);



